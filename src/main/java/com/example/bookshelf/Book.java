package com.example.bookshelf;

public class Book {
    private long id;
    private String people;
    private String author;
    private Integer pagesSum;
    private Integer yearOfPublished;
    private String publishingHouse;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getPagesSum() {
        return pagesSum;
    }

    public void setPagesSum(Integer pagesSum) {
        this.pagesSum = pagesSum;
    }

    public Integer getYearOfPublished() {
        return yearOfPublished;
    }

    public void setYearOfPublished(Integer yearOfPublished) {
        this.yearOfPublished = yearOfPublished;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }
}
